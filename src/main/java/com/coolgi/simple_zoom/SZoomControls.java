package com.coolgi.simple_zoom;

import com.badlogic.gdx.Input;
import finalforeach.cosmicreach.settings.Keybind;

public class SZoomControls {
    public static final Keybind zoomKeybind = Keybind.fromDefaultKey("zoom", Input.Keys.C);
}
