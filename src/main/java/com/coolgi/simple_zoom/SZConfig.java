package com.coolgi.simple_zoom;

import org.quiltmc.config.api.ReflectiveConfig;
import org.quiltmc.config.api.annotations.Comment;
import org.quiltmc.config.api.annotations.IntegerRange;
import org.quiltmc.config.api.values.TrackedValue;
import org.quiltmc.loader.api.config.v2.QuiltConfig;

public class SZConfig extends ReflectiveConfig {
    public static final SZConfig INSTANCE = QuiltConfig.create("", "simple_zoom", SZConfig.class);

    @Comment("The new fov set when you zoom. Lower numbers mean more zoom")
    public final TrackedValue<Float> zoomFov = this.value(10f);
}
