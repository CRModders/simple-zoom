package com.coolgi.simple_zoom.mixins;

import com.coolgi.simple_zoom.SZConfig;
import com.coolgi.simple_zoom.SZoomControls;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import finalforeach.cosmicreach.gamestates.InGame;
import finalforeach.cosmicreach.settings.FloatSetting;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(InGame.class)
public class FovModifierMixin {
    @WrapOperation(method = "update(F)V", at = @At(value = "INVOKE", target = "Lfinalforeach/cosmicreach/settings/FloatSetting;getValue()F"))
    private float getFov(FloatSetting instance, Operation<Float> original) {
        if (SZoomControls.zoomKeybind.isPressed())
            return SZConfig.INSTANCE.zoomFov.getRealValue().floatValue();
        else
            return original.call(instance);
    }

}
