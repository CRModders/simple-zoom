# Simple Zoom
> A simple zoom mod made for Cosmic Reach

To change zoom keybind, look in the keybinds menu\
To change the zoom amount, edit `config/simple_zoom.toml`

For instructions on how to install Cosmic Quilt mods, please visit the [Cosmic Quilt wiki](https://codeberg.org/CRModders/cosmic-quilt/wiki/User_Guide.md)


## NOTE: THIS IS AN EXAMPLE
This mod was made to be an example of how to use Quilt's config system and make a basic mod